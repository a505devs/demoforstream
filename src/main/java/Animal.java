public abstract class Animal implements Moveable {
    private double weight = 70;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public abstract void work();
}

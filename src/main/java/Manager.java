public class Manager extends Person {

    Manager(String fio, double salary, String speciality) {
        super(fio, salary, speciality);
    }

    void manage() {
        System.out.println(fio + " управляет твоим разумом, беги!");
    }

    @Override
    public void work() {
        System.out.println("Менеджер всегда работает эффективно, как вы посмели в этом сомневаться?");
    }
}
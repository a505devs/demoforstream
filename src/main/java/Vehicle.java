public abstract class Vehicle implements Moveable {
    protected double horsePower;

    protected Vehicle(double horsePower) {
        this.horsePower = horsePower;
    }

    public abstract void vroomVroom();
}

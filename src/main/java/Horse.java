public class Horse extends Animal {
    private String name;

    public Horse(String name) {
        this.name = name;
    }

    @Override
    public void work() {
        System.out.println("Лошадь " + this.name + " везет Ведьмака");
    }

    @Override
    public void move() {
        System.out.println("Тыгыдык Тыгыдык на крышу в текстуры");
    }
}

public class DeathStar extends Vehicle {
    protected DeathStar(double horsePower) {
        super(horsePower);
    }

    @Override
    public void vroomVroom() {
        System.out.println("Астрологи объявили выстрел по Альдерану мощностью " + this.horsePower + " л.с.! " +
                "Количество плачущих Лей увеличилось вдвое!");
    }

    @Override
    public void move() {
        System.out.println("Прыгаем на Явин-4!");
    }
}

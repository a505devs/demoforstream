public class Car extends Vehicle {
    protected Car(double horsePower) {
        super(horsePower);
    }

    @Override
    public void vroomVroom() {
        System.out.println("Доминик Торетто уважает можность этой тачки! Целых " + this.horsePower + " л.с.!");
    }

    @Override
    public void move() {
        System.out.println("Машинка едет!");
    }
}

public class Application {
    public static void main(String[] args) {
        Person john = new Person("Jonh Week", 10000, "Baba Yaga");
        Person loki = new Person("Loki Lafeison", 10000, "Lie God");
        Manager elon = new Manager("Elon Tusk", 1000000, "The Tuskla Company Leader");
        Horse plotva = new Horse("Plotva");

        Car dodge = new Car(10000);
        DeathStar starlight = new DeathStar(1000000000);

        Moveable[] somethingThatCanMove = { john, loki, elon, plotva, dodge, starlight };
        for(int i = 0; i < somethingThatCanMove.length; i++) {
            somethingThatCanMove[i].move();
        }
    }
}

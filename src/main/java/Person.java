public class Person extends Animal {
    String fio;
    double salary;
    String speciality;

    private boolean isEfficient = true;

    Person(String fio, double salary, String speciality) {
        this.fio = fio;
        this.salary = salary;
        this.speciality = speciality;
    }

    public boolean getLazyness() {
        return this.isEfficient;
    }

    public void setLazyness(boolean isLazy) {
        this.isEfficient = !isLazy;
    }

    public void work() {
        if(isEfficient)
            System.out.println("У " + fio + " скоро обед, приходите позже!!!!");
        else
            System.out.println(fio + " сейчас безвольное бревно!");
    }

    @Override
    public void move() {
        System.out.println("Ну, допустим, ходим!");
    }
}